@extends("layouts.master")

@section("titulo")
    Inicio
@endsection

@section("contenido")
    <div class="row">
        @foreach( $futbolistas as  $futbolista )
            <div class="col-xs-12 col-sm-6 col-md-4 mb-4">
                <div class="card text-white bg-dark" style="width: auto;">
                    <a href="{{ route('futbolistas.show' , $futbolista->id) }}">
                        <img class="card-img-top" src="{{asset('assets/img')}}/{{$futbolista->imagen}}" alt="{{$futbolista->nombre}}"
                             style="height:400px"/>
                    </a>
                    <div class="card-body">
                        <h4 class="card-title text-center">{{$futbolista->nombre}}</h4>
                    </div>
                    <ul class="list-group list-group-flush text-dark">
                        <li class="list-group-item">Pierna: {{$futbolista->pierna}}</li>
                        <li class="list-group-item">Peso: {{$futbolista->peso}}kg</li>
                        <li class="list-group-item">Altura: {{$futbolista->altura}}cm</li>
                        <li class="list-group-item">Edad actual: {{$futbolista->getEdad()}}</li>
                    </ul>
                </div>
            </div>
        @endforeach
    </div>
@endsection
