@extends('layouts.master')
@section('titulo')
    Crear
@endsection

@section("contenido")
    <div class="row">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-6">
                    <form class="formulario" action="{{route('futbolistas.store')}}" method="post" role="form">
                        <legend class="text-light bg-dark text-center h2">Introduce los datos del futbolista</legend>
                        @csrf
                        <label for="nombre">Nombre:</label>
                        <div class="form-group">
                            <input class="form-control mb-3" type="text" name="nombre" placeholder="Nombre del futbolista">
                        </div>
                        <label for="peso">Peso:</label>
                        <div class="form-group">
                            <input class="form-control mb-3" type="number" name="peso" placeholder="Peso en kg">
                        </div>
                        <label for="altura">Altura:</label>
                        <div class="form-group">
                            <input class="form-control mb-3" type="number" name="altura" placeholder="Altura en cm">
                        </div>
                        <label for="fecha">Fecha de nacimiento:</label>
                        <div class="form-group">
                            <input class="form-control d-inline mb-3" type="date" name="fechaNacimiento">
                        </div>
                        <label for="pierna">Pierna:</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="pierna"  value="derecha">
                            <label class="form-check-label" for="derecha">Derecha</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="pierna" value="izquierda">
                            <label class="form-check-label" for="izquierda">Izquierda</label>
                        </div>
                        <div class="form-check disabled">
                            <input class="form-check-input" type="radio" name="pierna"  value="ambidiestro">
                            <label class="form-check-label mb-3" for="ambidiestro">Ambidiestro</label>
                        </div>
                        <div class="form-group">
                            <label for="dorsal">Dorsal:</label>
                            <input class="form-control mb-3" type="number" name="dorsal" placeholder="Dorsal">
                        </div>
                        <div class="form-group">
                            <label for="imagen">Imagen</label>
                            <input type="file" class="form-control-file mt-3" name="imagen">
                        </div>
                        <input type="submit" name="insertar" class="btn btn-primary mt-3" value="Insertar futbolista">
                    </form>
                </div>


            </div>
        </div>

    </div>
@endsection