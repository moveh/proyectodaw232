<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FutbolistaController;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\LesionesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InicioController::class, 'inicio'])->name('home');

Route::get('futbolistas', [FutbolistaController::class, 'index'])->name('futbolistas.index');

Route::middleware(['auth:sanctum', 'verified'])->get('futbolistas/create', [FutbolistaController::class, 'create'])->name('futbolistas.create');

Route::get('futbolistas/{futbolista}', [FutbolistaController::class, 'show'])->name('futbolistas.show');

Route::middleware(['auth:sanctum', 'verified'])->get('futbolistas/{futbolista}/editar', [FutbolistaController::class, 'edit'])->name('futbolistas.edit');

Route::post('futbolistas/store', [FutbolistaController::class, 'store'])->name('futbolistas.store');

Route::put('futbolistas/{futbolista}', [FutbolistaController::class, 'update'])->name('futbolistas.update');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
