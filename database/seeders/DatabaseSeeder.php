<?php

namespace Database\Seeders;

use App\Models\Entrenador;
use App\Models\Lesiones;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Entrenador::factory(4)->create();

        DB::table('futbolistas')->delete();
        $this->call(FutbolistaSeeder::class);

        DB::table('users')->delete();
        $this->call(UserSeeder::class);
        \App\Models\User::factory(5)->create();

        DB::table('lesiones')->delete();
        $this->call(LesionesSeeder::class);

        Lesiones::factory(4)->create();
    }
}
