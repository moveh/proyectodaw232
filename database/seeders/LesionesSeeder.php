<?php

namespace Database\Seeders;

use App\Models\Lesiones;
use Illuminate\Database\Seeder;
use App\Models\Futbolista;

class LesionesSeeder extends Seeder
{
    private $lesiones = array(
        array(
            'fechaLesion' => '1997-02-08',
            'descripcion' => 'Ligamento lateral interno'
        ),
        array(
            'fechaLesion' => '2003-02-10',
            'descripcion' => 'Ligamento cruzado'
        ),
        array(
            'fechaLesion' => '2008-02-08',
            'descripcion' => 'Esguince de tobillo'
        ),
        array(
            'fechaLesion' => '2001-03-17',
            'descripcion' => 'Rotura de rodilla'
        )
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->lesiones as $lesion) {
            $a = new Lesiones();
            $a->fechaLesion = $lesion['fechaLesion'];
            // $a->slug = Str::slug($lesion['fechaLesion']);
            $a->descripcion = $lesion['descripcion'];
            $a->futbolista_id = Futbolista::all()->random()->id;
            $a->save();
        }
        $this->command->info('Tabla lesiones inicializada con datos');
    }
}
