<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{

    private $usuarios = array(
        array(
            'name' => 'Carlos',
            'email' => 'hola@gmail.com',
            'password' => 'carlos'
        ),
        array(
            'name' => 'Move',
            'email' => 'adios@gmail.com',
            'password' => 'move'
        )
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach ($this->usuarios as $usuario) {
            $a = new user();
            $a->name = $usuario['name'];
            // $a->slug = Str::slug($usuario['nombre']);
            $a->email = $usuario['email'];
            $a->password = bcrypt($usuario['password']);

            $a->save();
        }
        $this->command->info('Tabla usuarios inicializada con datos');
    }
}
