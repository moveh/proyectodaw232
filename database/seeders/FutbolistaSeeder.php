<?php

namespace Database\Seeders;

use App\Models\Entrenador;
use App\Models\Futbolista;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class FutbolistaSeeder extends Seeder
{

    private $futbolistas = array(
        array(
            'nombre' => 'Raúl González',
            'peso' => 75,
            'altura' => 180,
            'fechaNacimiento' => '1977-06-27',
            'imagen' => 'raul.jpg',
            'pierna' => 'izquierda',
            'dorsal' => 7
        ),
        array(
            'nombre' => 'Ronaldo Nazario',
            'peso' => 77,
            'altura' => 183,
            'fechaNacimiento' => '1976-09-22',
            'imagen' => 'ronaldo.jpg',
            'pierna' => 'derecha',
            'dorsal' => 9
        ),
        array(
            'nombre' => 'Cristiano Ronaldo',
            'peso' => 83,
            'altura' => 187,
            'fechaNacimiento' => '1985-02-05',
            'imagen' => 'cr.jpg',
            'pierna' => 'derecha',
            'dorsal' => 7
        ),
        array(
            'nombre' => 'Zinedine Zidane',
            'peso' => 78,
            'altura' => 185,
            'fechaNacimiento' => '1972-06-23',
            'imagen' => 'zidane.jpg',
            'pierna' => 'ambidiestro',
            'dorsal' => 5
        ),
        array(
            'nombre' => 'Lionel Messi',
            'peso' => 67,
            'altura' => 169,
            'fechaNacimiento' => '1987-06-24',
            'imagen' => 'messi.jpg',
            'pierna' => 'izquierda',
            'dorsal' => 10
        ),

        array(
            'nombre' => 'Luka Modrić',
            'peso' => 66.2,
            'altura' => 172,
            'fechaNacimiento' => '1985-09-09',
            'imagen' => 'modric.jpg',
            'pierna' => 'derecha',
            'dorsal' => 10
        ),

        array(
            'nombre' => 'Ronaldinho',
            'peso' => 76,
            'altura' => 182,
            'fechaNacimiento' => '1980-03-21',
            'imagen' => 'ronaldinho.jpg',
            'pierna' => 'derecha',
            'dorsal' => 10
        ),

        array(
            'nombre' => 'Diego Armando Maradona',
            'peso' => 67,
            'altura' => 165,
            'fechaNacimiento' => '1960-10-30',
            'imagen' => 'maradona.jpg',
            'pierna' => 'izquierda',
            'dorsal' => 10
        )
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->futbolistas as $futbolista) {
            $a = new Futbolista();
            $a->nombre = $futbolista['nombre'];
            // $a->slug = Str::slug($futbolista['nombre']);
            $a->peso = $futbolista['peso'];
            $a->altura = $futbolista['altura'];
            $a->fechaNacimiento = $futbolista['fechaNacimiento'];
            $a->imagen = $futbolista['imagen'];
            $a->pierna = $futbolista['pierna'];
            $a->dorsal = $futbolista['dorsal'];
            $a->save();
            $a->entrenadores()->attach([
                Entrenador::all()->skip(0)->take(4)->random()->id,
                //añadir otro como el de encima si quiero tener dos entrenadores
            ]);
        }
        $this->command->info('Tabla futbolistas inicializada con datos');
    }
}
