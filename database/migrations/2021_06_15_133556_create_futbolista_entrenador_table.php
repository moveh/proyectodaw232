<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFutbolistaEntrenadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entrenador_futbolista', function (Blueprint $table) {
            $table->id();
            $table->foreignId('futbolista_id');
            $table->foreign('futbolista_id')->references('id')->on('futbolistas');
            $table->foreignId('entrenador_id');
            $table->foreign('entrenador_id')->references('id')->on('entrenadores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entrenador_futbolista');
    }
}
