<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLesionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesiones', function (Blueprint $table) {
            $table->id();
            $table->date('fechaLesion');
            $table->string('descripcion', 32);
            $table->foreignId('futbolista_id');
            $table->foreign('futbolista_id')->references('id')->on('futbolistas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesiones');
    }
}
