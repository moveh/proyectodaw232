<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFutbolistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('futbolistas', function (Blueprint $table) {
            $table->id();
            $table->String('nombre');
            $table->double('peso', 6, 1);
            $table->double('altura', 6, 1);
            $table->date('fechaNacimiento');
            $table->string('imagen')->nullable();
            $table->string('pierna', 20)->nullable();
            $table->integer('dorsal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('futbolistas');
    }
}
