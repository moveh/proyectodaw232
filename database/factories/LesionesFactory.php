<?php

namespace Database\Factories;

use App\Models\Lesiones;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Futbolista;

class LesionesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Lesiones::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fechaLesion' => $this->faker->date,
            'descripcion' => 'descripcion general',
            'futbolista_id' => Futbolista::all()->random()->id,
        ];
    }
}
