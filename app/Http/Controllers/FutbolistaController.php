<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Futbolista;
use Database\Seeders\FutbolistaSeeder;
use Illuminate\Support\Facades\DB;

class FutbolistaController extends Controller
{
    //

    function index()
    {
        $futbolistas = Futbolista::all();
        return view('futbolistas.index')->with('futbolistas', $futbolistas);
    }


    function show(Futbolista $futbolista)
    {

        return view('futbolistas.show', ['futbolista' => $futbolista]);
    }

    function create()
    {
        return view('futbolistas.create');
    }

    function edit(Futbolista $futbolista)
    {
        //$futbolista = Futbolista::find($jugador);
        return view('futbolistas.edit', ['futbolista' => $futbolista]);
    }

    public function store(Request $a)
    {

        $futbolista = new futbolista();
        $futbolista->nombre = $a->nombre;
        $futbolista->peso = $a->peso;
        $futbolista->altura = $a->altura;
        $futbolista->fechaNacimiento = $a->fechaNacimiento;
        $futbolista->pierna = $a->pierna;
        $futbolista->dorsal = $a->dorsal;
        $futbolista->imagen = $a->imagen;
        $futbolista->save();

        return redirect()->route('futbolistas.show', $futbolista);
    }

    public function update(Request $a, int $id)
    {
        $futbolista = Futbolista::find($id);
        $futbolista->nombre = $a->nombre;
        $futbolista->peso = $a->peso;
        $futbolista->altura = $a->altura;
        $futbolista->fechaNacimiento = $a->fechaNacimiento;
        $futbolista->pierna = $a->pierna;
        $futbolista->dorsal = $a->dorsal;
        if ($a->imagen !== null) {
            $futbolista->imagen = $a->imagen->storeAs("", $a->imagen->getClientOriginalName());
        }
        $futbolista->save();

        return redirect()->route('futbolistas.index');
    }
}
