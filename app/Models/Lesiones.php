<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesiones extends Model
{
    use HasFactory;
    protected $table = 'lesiones';
}
