<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Futbolista extends Model
{
    use HasFactory;
    protected $table = 'futbolistas';

    public function getEdad()
    {
        $fechaFormateada = Carbon::parse($this->fechaNacimiento);
        return $fechaFormateada->diffInYears(Carbon::now());
    }

    public function lesiones()
    {
        return $this->hasMany(Lesiones::class);
    }

    public function entrenadores()
    {
        return $this->belongsToMany(Entrenador::class);
    }
}
